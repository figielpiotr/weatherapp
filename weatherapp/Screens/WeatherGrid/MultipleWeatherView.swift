//
//  MultipleWeatherView.swift
//  weatherapp
//
//  Created by Piotr Figiel on 07/01/2021.
//  Copyright © 2021 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit


class Statics {
    static let weatherViewCellID = "WeatherViewCell"
}

typealias EmptyClosure = (()->())

// MARK: - Multiple Weather View
class MultipleWeatherView: UIView {
    
    // MARK: Closures
    var onAddButtonTouched: EmptyClosure?
    
    // MARK: Views
    var collectionView: UICollectionView!
    
    init(){
        super.init(frame: CGRect.zero)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
}

// MARK: - Setup
private extension MultipleWeatherView {
    
    func setup() {
        setupCollectionView()
        setupAddButton()
    }
    
    func setupCollectionView() {
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 20, left: 10, bottom: 10, right: 10)
        layout.itemSize = CGSize(width: 70, height: 70)
        //layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        collectionView = UICollectionView(frame: frame, collectionViewLayout: layout)
        collectionView.backgroundColor = .white
        
        collectionView.register(WeatherViewCell.self, forCellWithReuseIdentifier: Statics.weatherViewCellID)
        
        addSubview(collectionView)
        collectionView.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
    }
    
    func setupAddButton() {
        let addButton = UIButton()
        addButton.backgroundColor = .green
        addButton.setTitle("+", for: .normal)
        addButton.addTarget(self, action: #selector(addButtonTouched), for: .touchUpInside)
        addSubview(addButton)
        addButton.snp.makeConstraints { (make) in
            make.height.equalTo(50)
            make.width.equalTo(50)
            make.bottom.equalToSuperview()
            make.centerX.equalToSuperview()
        }
    }
    
    @objc func addButtonTouched() {
        onAddButtonTouched?()
    }
}
