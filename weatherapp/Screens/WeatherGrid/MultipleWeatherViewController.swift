//
//  MultipleWeatherViewController.swift
//  weatherapp
//
//  Created by Piotr Figiel on 07/01/2021.
//  Copyright © 2021 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit
import SnapKit


class MultipleWeatherViewController: UIViewController {
    
    var viewModel = MultipleWeatherViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViewModel()
        setupView()
    }
}

// MARK: - Setup
extension MultipleWeatherViewController {
    
    func setupViewModel() {
        viewModel.onWeatherDataUpdate = { [weak self] in
            self?.customView.collectionView.reloadData()
        }
    }
    
    func setupView() {
        customView.collectionView.dataSource = self
        customView.collectionView.delegate = self
        
        customView.onAddButtonTouched = { [weak self] in
            let vc = SearchViewController()
            
            vc.onCityPicked = { [weak self] city in
                self?.viewModel.add(city: city)
            }
            
            self?.present(vc, animated: true, completion: nil)
        }
    }
}

// MARK: -
extension MultipleWeatherViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.weatherData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Statics.weatherViewCellID, for: indexPath)
        guard let weatherCell = cell as? WeatherViewCell else { return cell }
        weatherCell.configure(weather: viewModel.weatherData[indexPath.row])
        return weatherCell
    }
}

extension MultipleWeatherViewController: UICollectionViewDelegate {
    
}

// MARK: - View Programmatically
extension MultipleWeatherViewController: HasCustomView {
    
    // MARK: Custom View
    typealias CustomView = MultipleWeatherView
    
    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
}
