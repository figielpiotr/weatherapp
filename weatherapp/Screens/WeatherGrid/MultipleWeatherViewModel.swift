//
//  MultipleWeatherViewModel.swift
//  weatherapp
//
//  Created by Piotr Figiel on 07/01/2021.
//  Copyright © 2021 Piotr Figiel. All rights reserved.
//

import Foundation

class MultipleWeatherViewModel {
    
    // MARK: Closures
    var onWeatherDataUpdate: EmptyClosure?
    
    // MARK: Services
    var dataService: DataService
    var userDefaultsService: UserDefaultsService
    
    // MARK: Properties
    var weatherData: [WeatherData] = [] {
        didSet {
            onWeatherDataUpdate?()
        }
    }
    
    init(_ dataService: DataService = DataService.shared, _ userDefaultsService: UserDefaultsService = UserDefaultsService.shared) {
        self.dataService = dataService
        self.userDefaultsService = userDefaultsService
    }
    
    func loadData() {
        let cityIds = userDefaultsService.getSavedCityIds()
        
        
    }
    
    func add(city: City) {
        let emptyData = WeatherData(base: "loading", weather: [])
        self.weatherData.append(emptyData)
        
        dataService.getWeatherFor(lat: city.lat, lon: city.lon) { [weak self] (data, _) in
            guard let data = data else { return }
            if let index = self?.weatherData.firstIndex(of: emptyData) {
                self?.weatherData.remove(at: index)
            }
            self?.weatherData.append(data)
        }
    }
//    50.0833, 19.9167
//    52.2298, 21.0118
//    52.4069, 16.9299
//    54.3521, 18.6464
}
