//
//  WeatherViewCell.swift
//  weatherapp
//
//  Created by Piotr Figiel on 07/01/2021.
//  Copyright © 2021 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class WeatherViewCell: UICollectionViewCell {
    
    var label = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        backgroundColor = UIColor(red: .random(in: 0...1), green: .random(in: 0...1), blue: .random(in: 0...1), alpha: 1.0)
        addSubview(label)
        label.text = "test"
        label.snp.makeConstraints { (make) in
            make.edges.equalToSuperview()
        }
        setNeedsUpdateConstraints()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//    }
//
    func configure(weather: WeatherData) {
        label.text = weather.getCityName()
        if weather.base == "loading" {
            label.text = "#"
        }
        
    }
}
