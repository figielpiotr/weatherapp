//
//  ViewController.swift
//  weatherapp
//
//  Created by Piotr Figiel on 01/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit
import CoreLocation
import CoreData


class SingleWeatherViewController: UIViewController, CLLocationManagerDelegate, HasCustomView, UISearchResultsUpdating, UISearchBarDelegate {

    typealias CustomView = MainView

    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
    
    var dataService: DataService!
    var cityService: CityService!
    var locationService: LocationService!

    lazy var userDefaultsService: UserDefaultsService = { return UserDefaultsService.shared } ()
    
    
    var searchController = UISearchController(searchResultsController: SearchResultViewController())

    
    init(_ dataService: DataService = DataService.shared, _ cityService: CityService = CityService.shared, _ locationService: LocationService = LocationService.shared) {
        super.init(nibName: nil, bundle: nil)
        self.dataService = dataService
        self.cityService = cityService
        self.locationService = locationService
        
        //configuring cities database
        DispatchQueue.global(qos: .background).async { [weak self] in
            guard let self = self, let cityService = self.cityService else { return }
            do {
                try cityService.configureCitiesDatabase()
            } catch {
                fatalError(error.localizedDescription)
            }
        }
        
        self.customView.delegate = self
        
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Sercz"
        searchController.searchBar.isHidden = true
        searchController.searchBar.delegate = self
        searchController.isActive = false
        navigationItem.searchController = searchController
        
        definesPresentationContext = true
        navigationItem.hidesSearchBarWhenScrolling = true
        
        self.extendedLayoutIncludesOpaqueBars = true
        self.edgesForExtendedLayout = .all
        
        
        
        
        
        let swipeDownGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeDown(_:)))
        swipeDownGestureRecognizer.direction = .down
        self.view.addGestureRecognizer(swipeDownGestureRecognizer)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc func handleSwipeDown(_ sender:UISwipeGestureRecognizer) {
        if sender.direction == .down && !searchController.isActive {
            showSearchBar()
        }
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        hideSearchBar()
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        hideSearchBar()
    }
    
    func showSearchBar() {
        searchController.searchBar.becomeFirstResponder()
        print("tu kurwa: \(searchController.searchBar.isFirstResponder)")
        searchController.searchBar.isHidden = false
    }
    
    @objc func hideSearchBar() {
        print("hide")
        searchController.searchBar.isHidden = true
        searchController.searchBar.resignFirstResponder()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationService.locationManager?.requestAlwaysAuthorization()
        locationService.setup()
        self.reload()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
         self.customView.changeOrientation(orientation: UIApplication.shared.windows.first?.windowScene?.interfaceOrientation ?? UIInterfaceOrientation.portrait)
    }
    
    
    @objc func reload() {
        var coord = userDefaultsService.getSavedCoordinates()
        if coord == nil {
            let location = locationService.getCurrentLocation()
            guard let lon = location?.coordinate.longitude, let lat = location?.coordinate.latitude else { return }
            coord = Coord(lon: lon, lat: lat)
        }
        
        if let dataService = self.dataService, let coord = coord {
            self.customView.showSpinner()
            dataService.getWeatherFor(lat: coord.lat, lon: coord.lon) {[weak self] (data, error) in
                if let data = data, let icon = data.weather.first?.icon {
                    self?.customView.setWeatherImage(icon)
                    self?.customView.setWeatherData(cityName: data.getCityName(), temperature: data.getTemperature(), wind: data.getWind(), pressure: data.getPressure(), sunrise: data.getSunrise(), sunset: data.getSunset())
                }
                self?.customView.hideSpinner()
            }
        }
    }
    
    @objc func cancel() {
        if let dataService = self.dataService {
            dataService.cancelRequests()
        }
    }
    
    override func willTransition(to newCollection: UITraitCollection, with coordinator: UIViewControllerTransitionCoordinator) {
        coordinator.animate(alongsideTransition: { [weak self] context in
            guard let self = self else { return }
            guard let orientation = UIApplication.shared.windows.first?.windowScene?.interfaceOrientation else { return }
            self.customView.changeOrientation(orientation: orientation)
        })
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = searchController.searchBar
        guard let query = searchBar.text, query.count > 0 else { return }
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            if let result = self?.cityService?.searchCities(query: query, location: self?.locationService.getCurrentLocation()) {
                DispatchQueue.main.async {
                    if let resultController = self?.searchController.searchResultsController as? SearchResultViewController {
                        resultController.cities = result
                        //resultController.mainViewController = self
                        resultController.tableView.reloadData()
                    }
                }
            }
        }
    }
 
    func setCurrentLocation(lon: Double, lat: Double) {
        userDefaultsService.saveCoordinates(coord: Coord(lon: lon, lat: lat))
        self.reload()
    }
}

