//
//  MainView.swift
//  weatherapp
//
//  Created by Piotr Figiel on 06/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

class MainView: UIView {

    weak var _delegate: SingleWeatherViewController?
    var delegate: SingleWeatherViewController? {
        get {
            return _delegate
        }
        set {
            _delegate = newValue
            self.setupActions()
        }
    }
    
    var wrapper = UIStackView()
    var spinner = UIActivityIndicatorView()
    
    var cityName = UIButton()
    var placeholderImage = UIImage(named: "placeholder")
    var weatherImageView = UIImageView()
     
    var tempValueLabel = UILabel()
    var windValueLabel = UILabel()
    var pressureValueLabel = UILabel()
    var sunriseValueLabel = UILabel()
    var sunsetValueLabel = UILabel()
    
    var tempTitleLabel = UILabel()
    var windTitleLabel = UILabel()
    var pressureTitleLabel = UILabel()
    var sunriseTitleLabel = UILabel()
    var sunsetTitleLabel = UILabel()
    
    var infoBox = UIStackView()
    
    let reloadButton = UIButton()
    let cancelButton = UIButton()

    let search = UITextField()
    
    init(){
        super.init(frame: CGRect.zero)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
}

//MARK:- Setup
extension MainView {
    func setup() {
        self.backgroundColor = .white
        weatherImageView.image = placeholderImage
        self.setupSpinner()
        self.setupWrapper()
        //self.setupSearchField()
        self.setupCityNameButton()
        self.setupWeatherImage()
        //self.setupReloadButton()
        //self.setupCancelButton()
        self.setupInfoBox()
    }
}


//MARK:- Wrapper setup
extension MainView {
    //TODO: take into consideration big iphones dent
    private func setupWrapper() {
        wrapper.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(wrapper)
        let constraints = [
            wrapper.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.9),
            wrapper.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.9),
            wrapper.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            wrapper.centerYAnchor.constraint(equalTo: self.centerYAnchor),
        ]
        constraints.forEach({$0.isActive = true})
        wrapper.axis = .vertical
        wrapper.alignment = .fill
        wrapper.distribution = .equalSpacing
    }
}

//MARK:- Spinner setup
extension MainView {
    private func setupSpinner() {
        spinner.stopAnimating()
        spinner.isHidden = true
        spinner.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(spinner)
        let constraints = [
            spinner.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            spinner.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            spinner.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.1),
            spinner.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.1),
        ]
        constraints.forEach({$0.isActive = true})
    }
    
    func showSpinner() {
        self.spinner.startAnimating()
        self.spinner.isHidden = false
        self.wrapper.isHidden = true
    }
    
    func hideSpinner() {
        self.spinner.isHidden = true
        self.wrapper.isHidden = false
        self.spinner.stopAnimating()
    }
}




//MARK:- Weather Image setup
extension MainView {
    private func setupWeatherImage(orientation: UIInterfaceOrientation = .portrait) {
        weatherImageView.translatesAutoresizingMaskIntoConstraints = false
        weatherImageView.contentMode = .scaleAspectFit
        weatherImageView.isUserInteractionEnabled = true
    }

    func setWeatherImage(_ iconCode: String) {
        let range = NSRange(location: 0, length: iconCode.utf16.count)
        let regex = try! NSRegularExpression(pattern: "(^[0-9][0-9])(n|d)")
        if regex.firstMatch(in: iconCode, options: [], range: range) != nil {
            self.weatherImageView.image = UIImage(named: iconCode)
        } else {
            self.weatherImageView.image = self.placeholderImage
        }
    }
}

//MARK:- City name button setup
extension MainView  {
    private func setupCityNameButton() {
        self.cityName.translatesAutoresizingMaskIntoConstraints = false
        self.cityName.setTitle("Krakow", for: .normal)
        self.cityName.setTitleColor(.black, for: .normal)
        self.cityName.setTitleColor(.red, for: .highlighted)
    }
}


//MARK:- Infobox setup
extension MainView {
    private func setupInfoBox() {
        infoBox.translatesAutoresizingMaskIntoConstraints = false
        infoBox.axis = .vertical
        infoBox.alignment = .fill
        infoBox.distribution = .equalSpacing
        
        tempTitleLabel.text = "Temperature:"
        windTitleLabel.text = "Wind:"
        pressureTitleLabel.text = "Pressure:"
        sunriseTitleLabel.text = "Sunrise:"
        sunsetTitleLabel.text = "Sunset:"
        
        tempValueLabel.text = "32.42 C"
        windValueLabel.text = "6.2"
        pressureValueLabel.text = "1232"
        sunriseValueLabel.text = "5:32"
        sunsetValueLabel.text = "21:30"
        
        let infoBoxRows = [
            (tempTitleLabel, tempValueLabel),
            (windTitleLabel, windValueLabel),
            (pressureTitleLabel, pressureValueLabel),
            (sunriseTitleLabel, sunriseValueLabel),
            (sunsetTitleLabel, sunsetValueLabel),
        ]
        
        for labels in infoBoxRows {
            labels.0.translatesAutoresizingMaskIntoConstraints = false
            labels.1.translatesAutoresizingMaskIntoConstraints = false
            let row = UIStackView()
            row.addBackground(color: .red)
            row.translatesAutoresizingMaskIntoConstraints = false
            row.axis = .horizontal
            row.distribution = .equalSpacing
            labels.0.textAlignment = .left
            row.addArrangedSubview(labels.0)
            row.addArrangedSubview(labels.1)
            infoBox.addArrangedSubview(row)
        }
    }
    
    private func setupInfoBoxConstraints() {
        for case let row as UIStackView in infoBox.arrangedSubviews {
            row.widthAnchor.constraint(equalTo: infoBox.widthAnchor).isActive = true
            for (index, element) in row.arrangedSubviews.enumerated() {
                element.heightAnchor.constraint(equalTo: row.heightAnchor, multiplier: 1).isActive = true
                if index%2 == 0 {
                    element.widthAnchor.constraint(equalTo: row.widthAnchor, multiplier: 0.5).isActive = true
                } else {
                    element.heightAnchor.constraint(equalToConstant: 20).isActive = true
                }
            }
        }
    }
}

//MARK:- Setup Search Field
extension MainView {
    //TODO: to be done properly as it's not working on landscape view
    private func setupSearchField() {
        search.translatesAutoresizingMaskIntoConstraints = false
        wrapper.addArrangedSubview(search)
//        let constraints = [
//            search.trailingAnchor.constraint(equalTo: wrapper.trailingAnchor),
//            search.leadingAnchor.constraint(equalTo: wrapper.leadingAnchor),
//        ]
//        constraints.forEach({$0.isActive = true})

        //field.font = UIFont.systemFont(ofSize: 15)
        search.borderStyle = UITextField.BorderStyle.roundedRect
        search.autocorrectionType = UITextAutocorrectionType.no
        search.keyboardType = UIKeyboardType.default
        search.returnKeyType = UIReturnKeyType.done
        search.clearButtonMode = UITextField.ViewMode.whileEditing
        search.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
    }
}



//MARK:- Buttons actions
extension MainView {
    private func setupActions() {
        print("setup actions")
        if let delegate = self.delegate {
            let tap = UITapGestureRecognizer(target: delegate, action: #selector(delegate.reload))
            weatherImageView.addGestureRecognizer(tap)
        }
    }
}

//MARK:- Orientation listeners
extension MainView {
    @objc func changeOrientation(orientation: UIInterfaceOrientation) {
        var constraints: [NSLayoutConstraint] = []
        //Remove all constraints as we need new set of them
        wrapper.removeConstraints(wrapper.constraints)
        weatherImageView.removeConstraints(weatherImageView.constraints)
        cityName.removeConstraints(cityName.constraints)
        infoBox.removeConstraints(infoBox.constraints)
        
        //clear remove all the elements from wrapper as we need new layout
        wrapper.removeArrangedSubview(weatherImageView)
        wrapper.removeArrangedSubview(cityName)
        wrapper.removeArrangedSubview(infoBox)
        infoBox.removeArrangedSubview(cityName)
        
        
        if orientation.isLandscape {
            //WRAPPER
            wrapper.axis = .horizontal

            //IMAGE
            wrapper.addArrangedSubview(weatherImageView)
            constraints.append(weatherImageView.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 0.45))
            constraints.append(weatherImageView.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 1))
            
            //INFOBOX
            wrapper.addArrangedSubview(infoBox)
            constraints.append(infoBox.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 0.45))
            constraints.append(infoBox.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.6))
            setupInfoBoxConstraints()
            
            //CITY NAME to infobox at first position
            infoBox.insertArrangedSubview(cityName, at: 0)
        } else {
            //WRAPPER
            wrapper.axis = .vertical
            
            //CITY NAME
            wrapper.addArrangedSubview(cityName)
            constraints.append(self.cityName.heightAnchor.constraint(equalToConstant: 10))
            constraints.append(self.cityName.widthAnchor.constraint(equalTo: wrapper.widthAnchor))

            //IMAGE
            wrapper.addArrangedSubview(weatherImageView)
//            constraints.append(weatherImageView.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 0.70))
            constraints.append(weatherImageView.heightAnchor.constraint(equalTo: weatherImageView.widthAnchor, multiplier: 0.6))

            //INFO BOX
            wrapper.addArrangedSubview(infoBox)
            constraints.append(infoBox.widthAnchor.constraint(equalTo: wrapper.widthAnchor, multiplier: 1))
            constraints.append(infoBox.heightAnchor.constraint(equalTo: wrapper.heightAnchor, multiplier: 0.3))
            setupInfoBoxConstraints()
        }
        constraints.forEach({$0.isActive = true})
    }
}


//MARK:- Setting data values
extension MainView {
    func setWeatherData(cityName: String, temperature: String, wind: String, pressure: String, sunrise: String, sunset: String) {
        self.cityName.setTitle(cityName, for: .normal)
        self.tempValueLabel.text = temperature
        self.windValueLabel.text = wind
        self.pressureValueLabel.text = pressure
        self.sunriseValueLabel.text = sunrise
        self.sunsetValueLabel.text = sunset
    }
}


    
    
//    func setupReloadButton() {
////        self.reloadButton.translatesAutoresizingMaskIntoConstraints = false
////        self.addSubview(reloadButton)
////        var constraints = [NSLayoutConstraint]()
////        constraints.append(self.reloadButton.heightAnchor.constraint(equalToConstant: 30))
////        constraints.append(self.reloadButton.leftAnchor.constraint(equalTo: self.leftAnchor))
////        constraints.append(self.reloadButton.rightAnchor.constraint(equalTo: self.rightAnchor))
////        //TODO: take into consideration big iphones dent
////        constraints.append(self.reloadButton.bottomAnchor.constraint(equalTo: self.bottomAnchor))
////        for c in constraints {
////            c.isActive = true
////        }
////        self.reloadButton.setTitle("Reload", for: .normal)
////        self.reloadButton.setTitleColor(.black, for: .normal)
////        self.reloadButton.setTitleColor(.red, for: .highlighted)
//    }
    

    

    
  
//    private func setupCancelButton() {
////        self.cancelButton.translatesAutoresizingMaskIntoConstraints = false
////        self.addSubview(cancelButton)
////        var constraints = [NSLayoutConstraint]()
////        constraints.append(self.cancelButton.heightAnchor.constraint(equalToConstant: 30))
////        constraints.append(self.cancelButton.leftAnchor.constraint(equalTo: self.leftAnchor))
////        constraints.append(self.cancelButton.rightAnchor.constraint(equalTo: self.rightAnchor))
////        //TODO: take into consideration big iphones dent
////        constraints.append(self.cancelButton.topAnchor.constraint(equalTo: self.topAnchor))
////        for c in constraints {
////            c.isActive = true
////        }
////        self.cancelButton.setTitle("Cancel", for: .normal)
////        self.cancelButton.setTitleColor(.black, for: .normal)
////        self.cancelButton.setTitleColor(.red, for: .highlighted)
//    }
    


    

//MARK:- To be removed
extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}
