//
//  CitySearchViewCell.swift
//  weatherapp
//
//  Created by Piotr Figiel on 11/08/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import UIKit

class CitySearchViewCell: UITableViewCell {

    let name = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    private func setup() {
        name.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(name)
        let _ = [
            name.trailingAnchor.constraint(equalTo: self.trailingAnchor),
            name.leadingAnchor.constraint(equalTo: self.leadingAnchor),
            name.topAnchor.constraint(equalTo: self.topAnchor),
            name.bottomAnchor.constraint(equalTo: self.bottomAnchor)
            ].map({$0.isActive = true})
        self.setNeedsUpdateConstraints()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(city: City) {
        name.text = "\(city.name ?? "") : \(city.country ?? "")"
    }
}
