//
//  MainViewController.swift
//  weatherapp
//
//  Created by Piotr Figiel on 07/01/2021.
//  Copyright © 2021 Piotr Figiel. All rights reserved.
//

import UIKit


class ColorView: UIView {
    
    init(){
        super.init(frame: CGRect.zero)
        //self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        //self.setup()
    }
}

class ColorViewController: UIViewController {
    
    typealias CustomView = ColorView
    
    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
    
    
    var backgroundColor: UIColor? {
        didSet {
            view.backgroundColor = backgroundColor
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

}


class DupaViewController: UIPageViewController {

    //memory!
    lazy var controllers: [UIViewController] = {
        return [
        MultipleWeatherViewController(),
        SingleWeatherViewController(),
        createViewController(color: .yellow)
        ]
    }()
    
    override init(transitionStyle style: UIPageViewController.TransitionStyle, navigationOrientation: UIPageViewController.NavigationOrientation, options: [UIPageViewController.OptionsKey : Any]? = nil) {
        super.init(transitionStyle: .scroll, navigationOrientation: navigationOrientation, options: options)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        
        
        
        if let controller = controllers.first {
            setViewControllers([controller], direction: .forward, animated: true, completion: nil)
        }
        
        
        let swipeDownGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(handleSwipeDown(_:)))
        swipeDownGestureRecognizer.direction = .down
        self.view.addGestureRecognizer(swipeDownGestureRecognizer)
    }
    
    func createViewController(color: UIColor) -> ColorViewController {
        let controller = ColorViewController()
        controller.backgroundColor = color
        return controller
    }
    
    @objc func handleSwipeDown(_ sender:UISwipeGestureRecognizer) {
        if let controller = controllers[1] as? SingleWeatherViewController {
            controller.showSearchBar()
//        if sender.direction == .down && !searchController.isActive {
//            showSearchBar()
//        }
        }
    }
}

// MARK: - UIPageViewControllerDataSource
extension DupaViewController: UIPageViewControllerDataSource {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = controllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard controllers.count > previousIndex else {
            return nil
        }
        
        return controllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = controllers.firstIndex(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = controllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        return controllers[nextIndex]
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return controllers.count
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        guard let firstViewController = controllers.first, let firstViewControllerIndex = controllers.firstIndex(of: firstViewController) else {
            return 0
        }
        
        return firstViewControllerIndex
    }
    
}
