//
//  SearchViewController.swift
//  weatherapp
//
//  Created by Piotr Figiel on 08/01/2021.
//  Copyright © 2021 Piotr Figiel. All rights reserved.
//

import Foundation
import UIKit
import SnapKit

class SearchView: UIView {
    
    // MARK: Closures
    var onSearchBarTextChanged: ((String)->())?
    
    // MARK: Subviews
    var searchBar = UITextField()
    var results = UITableView()
    
    init(){
        super.init(frame: CGRect.zero)
        self.setup()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setup()
    }
    
    func setup() {
        setupSearchBar()
        setupTableView()
    }
    
    func setupSearchBar() {
        searchBar.placeholder = "search"
        searchBar.addTarget(self, action: #selector(searchBarDidChange), for: .editingChanged)
        
        
        addSubview(searchBar)
        searchBar.snp.makeConstraints { (make) in
            make.width.equalToSuperview()
            make.height.equalTo(30)
            make.centerX.equalToSuperview()
            make.top.equalToSuperview()
        }
    }
    
    func setupTableView() {
        results.register(CitySearchViewCell.self, forCellReuseIdentifier: "CitySearchCell")
        
        addSubview(results)
        results.snp.makeConstraints { (make) in
            make.top.equalTo(searchBar.snp.bottom)
            make.leading.equalToSuperview()
            make.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
        }
    }
    
    @objc func searchBarDidChange() {
        guard let text = searchBar.text else { return }
        onSearchBarTextChanged?(text)
    }
}




class SearchViewModel {
    
    // MARK: Closures
    var onCitiesUpdate: EmptyClosure?
    
    // MARK: Services
    
    
    // MARK: Properties
    var cities: [City] = []
    
    func searchCity(name: String) {
        DispatchQueue.global(qos: .userInitiated).async { [weak self] in
            self?.cities = CityService.shared.searchCities(query: name, location: LocationService.shared.getCurrentLocation())
            DispatchQueue.main.async {
                self?.onCitiesUpdate?()
            }
        }
    }
}







class SearchViewController: UITableViewController {
    
    // MARK: Closures
    var onCityPicked: ((City)->())?
    
    // MARK: Properties
    let viewModel = SearchViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        setupViewModel()
    }
}


// MARK: Setup
extension SearchViewController {
    
    func setupView() {
        customView.results.dataSource = self
        customView.results.delegate = self
        
        customView.onSearchBarTextChanged = { [weak self] text in
            self?.viewModel.searchCity(name: text)
        }
    }
    
    func setupViewModel() {
        viewModel.onCitiesUpdate = { [weak self] in
            self?.customView.results.reloadData()
        }
    }
}

// MARK: description
extension SearchViewController {
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.cities.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CitySearchCell", for: indexPath) as! CitySearchViewCell
        cell.configure(city: viewModel.cities[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let city = viewModel.cities[indexPath.row]
        onCityPicked?(city)
        self.dismiss(animated: true)
    }
}

// MARK: - View Programmatically
extension SearchViewController: HasCustomView {
    
    // MARK: Custom View
    typealias CustomView = SearchView
    
    override func loadView() {
        view = CustomView()
        self.edgesForExtendedLayout = []
    }
}
