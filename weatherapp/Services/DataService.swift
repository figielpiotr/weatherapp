//
//  File.swift
//  weatherapp
//
//  Created by Piotr Figiel on 03/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import Foundation
import CoreData
import UIKit

enum AppError: Error {
    case serverIsNotResponding
    case requestCancelled
    case wrongData
    case defaultError
    case JSONdbFileNotFound
    case JSONfileCouldNotBeRead
    case JSONfileCouldNotBeParsed
    
    public var errorDescription: String? {
        switch self {
        case .serverIsNotResponding:
            return NSLocalizedString("There was problem with the connection", comment: "Connection Problem")
        case .requestCancelled:
            return NSLocalizedString("Request was cancelled", comment: "Request Cancelled")
        case .wrongData:
            return NSLocalizedString("There was problem with the weather data. Please try again.", comment: "Invalid Data")
        case .JSONdbFileNotFound:
            return NSLocalizedString("File not found.", comment: "JSON database file was not found")
        case .JSONfileCouldNotBeRead:
            return NSLocalizedString("File could not be read", comment: "JSON database file could not be read")
        case .JSONfileCouldNotBeParsed:
            return NSLocalizedString("JSON file could not be parsed.", comment: "Invalid Data")
        case .defaultError:
            return NSLocalizedString("An error occured. Sorry.", comment: "Default Error")
        }
    }
}

class DataService {
    
    static let shared = DataService()
    
    private var session: URLSession
    var dataTask: URLSessionDataTask?
    
    var cities: [NSManagedObject] = []
    
    
    init(session: URLSession = URLSession(configuration: URLSessionConfiguration.default)) {
        self.session = session
    }
    
    
      
    private var api_url = "https://api.openweathermap.org/data/2.5/weather"
    //TODO: move to safe place and change :)
    private var api_key = "a5b934e94bf3b6d9477c545ebba81733"
    
    //TODO: log api errors, do the error when serialization fails
    //TODO: 1. We recommend making calls to the API no more than one time every 10 minutes for one location (city / coordinates / zip-code). This is due to the fact that weather data in our system is updated no more than one time every 10 minutes.

    func getWeatherFor(lat: Double, lon: Double, onComplete: @escaping (WeatherData?, AppError?) -> ()) {
        dataTask?.cancel()
        
        if var urlComponents = URLComponents(string: api_url) {
            urlComponents.queryItems = [
                URLQueryItem(name: "lat", value: "\(lat)"),
                URLQueryItem(name: "lon", value: "\(lon)"),
                URLQueryItem(name: "appid", value: api_key)
            ]
            guard let url = urlComponents.url else { return }
            
            
            
            dataTask = session.dataTask(with: url, completionHandler: { [weak self] (data, response, error) in
                var finalError: AppError? = nil
                var weatherData: WeatherData? = nil
                defer {
                    self?.dataTask = nil
                }
                
                if let error = error as NSError? {
                    switch error.code {
                    case NSURLErrorTimedOut:
                        finalError = .serverIsNotResponding
                    case NSURLErrorCancelled:
                        finalError = .requestCancelled
                    default:
                        finalError = .defaultError
                    }
                    
                } else if let data = data, let response = response as? HTTPURLResponse, response.statusCode == 200 {
                    do {
                        weatherData = try JSONDecoder().decode(WeatherData.self, from: data)
                    } catch {
                        finalError = .wrongData
                    }
                }
                
                DispatchQueue.main.async {
                    onComplete(weatherData, finalError)
                }
            })
            dataTask?.resume()
        }
    }
    
    func getWeatherFor(cityIds: [Int], onComplete: @escaping (WeatherData?, AppError?) -> ()) {
        dataTask?.cancel()
        if var urlComponents = URLComponents(string: api_url) {
            urlComponents.queryItems = [
                URLQueryItem(name: "id", value: "\(cityIds)"),
                URLQueryItem(name: "appid", value: api_key)
            ]
            guard let url = urlComponents.url else { return }
        
    }
    
    func cancelRequests() {
        dataTask?.cancel()
    }
}
