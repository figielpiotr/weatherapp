//
//  CityService.swift
//  weatherapp
//
//  Created by Piotr Figiel on 17/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation

class CityService {
    
    static let shared = CityService()
    
    let backgroundContext: NSManagedObjectContext
    let mainContext: NSManagedObjectContext
    let bundle: Bundle
    let fileName: String
    
    
    init(backgroundContext: NSManagedObjectContext = CoreDataStack.shared.backgroundContext, mainContext: NSManagedObjectContext = CoreDataStack.shared.mainContext, bundle: Bundle = Bundle.main, resource: String = "current.city.list") {
        self.backgroundContext = backgroundContext
        self.mainContext = mainContext
        self.bundle = bundle
        self.fileName = resource
    }
    
    func addCity(id: Int, name: String, country: String, lon: Double, lat: Double) {
        backgroundContext.performAndWait {
            createCity(id: id, name: name, country: country, lon: lon, lat: lat)
            if self.backgroundContext.hasChanges {
                try? self.backgroundContext.save()
            }
        }
    }
    
    //TODO: to be moved to factory?
    private func createCity(id: Int, name: String, country: String, lon: Double, lat: Double) {
        let city = NSEntityDescription.insertNewObject(forEntityName: "City", into: self.backgroundContext) as! City
        city.country = country
        city.id = Int32(id)
        city.lat = lat
        city.lon = lon
        city.name = name
    }
      
    func configureCitiesDatabase() throws {
//        let previouslyLaunched =
//          UserDefaults.standard.bool(forKey: "previouslyLaunched")
//        if !previouslyLaunched {
//          UserDefaults.standard.set(true, forKey: "previouslyLaunched")
        
        if isCityDatabaseEmpty() {
            guard let path = self.bundle.path(forResource: self.fileName, ofType: "json") else { throw AppError.JSONdbFileNotFound }
            guard let data = try String(contentsOfFile: path).data(using: .utf8) else { throw AppError.JSONfileCouldNotBeRead }
            do {
                let cityData = try JSONDecoder().decode(CityData.self, from: data)
                for city in cityData {
                    self.createCity(id: city.id, name: city.name, country: city.country, lon: city.coord.lon, lat: city.coord.lat)
                }
            } catch {
                debugPrint(error)
                throw AppError.JSONfileCouldNotBeParsed
            }
                
            if self.backgroundContext.hasChanges {
                try? self.backgroundContext.save()
            }
            print("koniec")
        }
    }
    
    func isCityDatabaseEmpty() -> Bool {
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "City")
        var count = 0
        do {
            count = try mainContext.count(for: fetchRequest)
        } catch let error as NSError {
            //TODO: Proper exception handling
          print("Could not fetch. \(error), \(error.userInfo)")
        }
        return (count > 0) ? false : true
    }
    
    func searchCities(query: String, location: CLLocation?) -> [City]{
        var cities: [City] = []
        let fetchRequest = NSFetchRequest<NSManagedObject>(entityName: "City")
        fetchRequest.predicate = NSPredicate(format: "name LIKE[c] %@", argumentArray: ["\(query)*"])
        
        do {
            cities = try self.backgroundContext.fetch(fetchRequest) as? [City] ?? []
            if let location = location {
                cities.sort { (lCity, rCity) -> Bool in
                    return lCity.distanceTo(lon: location.coordinate.longitude, lat: location.coordinate.latitude) < rCity.distanceTo(lon: location.coordinate.longitude, lat: location.coordinate.latitude)
                }
            }
         } catch let error as NSError {
           print("Could not fetch. \(error), \(error.userInfo)")
         }
        return cities
    }
}




