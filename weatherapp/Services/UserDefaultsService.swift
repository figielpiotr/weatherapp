//
//  UserDataService.swift
//  weatherapp
//
//  Created by Piotr Figiel on 25/08/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import Foundation
import CoreLocation

class UserDefaultsService {
    static let shared = UserDefaultsService()
    let userDefaults = UserDefaults.standard
    
    let coordsKey = "savedCoords"
    let citiesKey = "cities"
    
    func saveCoordinates(coord: Coord) {
        do {
            try userDefaults.setObject(coord, forKey: coordsKey)
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func getSavedCoordinates() -> Coord? {
        do {
            let location = try userDefaults.getObject(forKey: coordsKey, castTo: Coord.self)
            return location
        } catch {
            print(error.localizedDescription)
        }
        return nil
    }

    func removeSavedCoordinate() {
        userDefaults.removeObject(forKey: coordsKey)
    }
    
    func saveCityIds(cities: [City]) {
        let ids = cities.map {
            $0.id
        }
        userDefaults.setValue(ids, forKey: citiesKey)
    }
    
    func addCity(_ city: City) {
        var cityIds = getSavedCityIds()
        cityIds.append(Int(city.id))
        userDefaults.setValue(cityIds, forKey: citiesKey)
    }
    
    func getSavedCityIds() -> [Int] {
        guard let cities = userDefaults.array(forKey: citiesKey) as? [Int] else { return [] }
        return cities
    }
}
