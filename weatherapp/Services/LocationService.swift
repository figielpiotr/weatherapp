//
//  LocationService.swift
//  weatherapp
//
//  Created by Piotr Figiel on 22/08/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import Foundation
import CoreLocation

class LocationService {
    static let shared = LocationService()

    var locationManager: CLLocationManager?
    var geocoder: CLGeocoder?
    
    var currentCountryCode: String?
    
    init(_ locationManager: CLLocationManager = CLLocationManager(),
         _ geocoder: CLGeocoder = CLGeocoder()
         ) {
        self.locationManager = locationManager
        self.geocoder = geocoder
    }

    func setup() {
        self.setupCurrentCountry()
    }
    
    func getCurrentLocation() -> CLLocation? {
        return locationManager?.location
    }
    
    private func setupCurrentCountry() {
        guard let location = self.locationManager?.location else { return }
        let geocoder = CLGeocoder()
        geocoder.reverseGeocodeLocation(location) { [weak self] (placemarks, error) in
            guard let placemarks = placemarks else { return }
            self?.currentCountryCode = placemarks.first?.isoCountryCode
        }
    }
    
}
