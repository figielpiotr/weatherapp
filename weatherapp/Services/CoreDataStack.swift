//
//  CoreDataService.swift
//  weatherapp
//
//  Created by Piotr Figiel on 17/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class CoreDataStack {
    
    static let shared = CoreDataStack()
    var storeType: String?
    
    lazy var persistentContainer: NSPersistentContainer! = {
        let persistentContainer = NSPersistentContainer(name: "weatherapp")
        if let storeType = storeType {
            let desc = persistentContainer.persistentStoreDescriptions.first
            desc?.type = storeType
        }
        return persistentContainer
    }()
    
    //read-write
    lazy var backgroundContext: NSManagedObjectContext = {
        let context = self.persistentContainer.newBackgroundContext()
        context.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        return context
    }()

    //read only
    lazy var mainContext: NSManagedObjectContext = {
        let context = self.persistentContainer.viewContext
        context.automaticallyMergesChangesFromParent = true
        return context
    }()
    
    func setup(storeType: String = NSSQLiteStoreType, completion: (() -> Void)?) {
        self.storeType = storeType
        loadPersistentStore {
            completion?()
        }
    }

    private func loadPersistentStore(completion: @escaping () -> Void) {
        //handle data migration on a different thread/queue here
        persistentContainer.loadPersistentStores { description, error in
            guard error == nil else {
                fatalError("was unable to load store \(error!)")
            }

            completion()
        }
    }
}
