//
//  Coord.swift
//  weatherapp
//
//  Created by Piotr Figiel on 22/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import Foundation

// MARK: - Coord
struct Coord: Codable, Equatable {
    let lon, lat: Double
}

// MARK: Coord convenience initializers and mutators

//extension Coord {
//    init(data: Data) throws {
//        self = try newJSONDecoder().decode(Coord.self, from: data)
//    }
//
//    init(_ json: String, using encoding: String.Encoding = .utf8) throws {
//        guard let data = json.data(using: encoding) else {
//            throw NSError(domain: "JSONDecoding", code: 0, userInfo: nil)
//        }
//        try self.init(data: data)
//    }
//
//    init(fromURL url: URL) throws {
//        try self.init(data: try Data(contentsOf: url))
//    }
//
//    func with(
//        lon: Double? = nil,
//        lat: Double? = nil
//    ) -> Coord {
//        return Coord(
//            lon: lon ?? self.lon,
//            lat: lat ?? self.lat
//        )
//    }
//
//    func jsonData() throws -> Data {
//        return try newJSONEncoder().encode(self)
//    }
//
//    func jsonString(encoding: String.Encoding = .utf8) throws -> String? {
//        return String(data: try self.jsonData(), encoding: encoding)
//    }
//}
