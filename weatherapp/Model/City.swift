//
//  City.swift
//  weatherapp
//
//  Created by Piotr Figiel on 22/08/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import Foundation

extension City {
    
    func distanceTo(lon: Double, lat: Double) -> Double {
        let earthR = 6371.0
        let dLat = deg2rad(self.lat - lat)
        let dLon = deg2rad(self.lon - lon)
        let a = sin(dLat/2) * sin(dLat/2) + cos(deg2rad(lat)) * cos(deg2rad(self.lat)) * sin(dLon/2) * sin(dLon/2)
        let c = 2 * atan2(sqrt(a), sqrt(1-a))
        let d = earthR * c
        return d
    }
    
    private func deg2rad(_ deg: Double) -> Double {
        return deg * (Double.pi/180)
    }
}
