//
//  WeatherData.swift
//  weatherapp
//
//  Created by Piotr Figiel on 14/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import Foundation

//Data structures for whats returned from weatherapi
struct Cloud: Codable, Equatable {
    var all: Int?
    enum CodingKeys: String, CodingKey {
        case all
    }
}

//struct Coord: Codable, Equatable {
//    var lat: Double?
//    var lon: Double?
//    
//    enum CodingKeys: String, CodingKey {
//        case lat
//        case lon
//    }
//}

struct Main: Codable, Equatable {
    var feels_like: Double?
    var humidity: Int?
    var pressure: Int?
    var temp: Double?
    var temp_max: Double?
    var temp_min: Double?
    
    enum CodingKeys: String, CodingKey {
        case feels_like
        case humidity
        case pressure
        case temp
        case temp_max
        case temp_min
    }
}

struct Sys: Codable, Equatable {
    var country: String?
    var id: Int?
    var sunrise: Int?
    var sunset: Int?
    var type: Int?
    
    enum CodingKeys: String, CodingKey {
        case country
        case id
        case sunrise
        case sunset
        case type
    }
}

struct Weather: Codable, Equatable {
    var description: String?
    var icon: String?
    var id: Int?
    var main: String?
    
    enum CodingKeys: String, CodingKey {
        case description
        case icon
        case id
        case main
    }
}

struct Wind: Codable, Equatable {
    var deg: Int?
    var speed: Double?
    
    enum CodingKeys: String, CodingKey {
        case deg
        case speed
    }
}

struct WeatherData: Codable, Equatable {
//    static func == (lhs: WeatherData, rhs: WeatherData) -> Bool {
//        return lhs.base == rhs.base &&
//        lhs.clouds == rhs.clouds &&
//        lhs.cod == rhs.cod &&
//        lhs.coord == rhs.coord &&
//        lhs.dt == rhs.dt &&
//        lhs.id == rhs.id &&
//        lhs.main == rhs.main &&
//        lhs.base == rhs.base &&
//        lhs.base == rhs.base &&
//        lhs.base == rhs.base &&
//        lhs.base == rhs.base &&
//        lhs.base == rhs.base &&
//        lhs.base == rhs.base &&
//        lhs.base == rhs.base &&
//        lhs.base == rhs.base &&
//        
//        
//    }
    
    var base: String;
    var clouds: Cloud?
    var cod: Int?
    var coord: Coord?
    var dt: Int?
    var id: Int?
    var main: Main?
    var name: String?
    var sys: Sys?
    var timezone: Int?
    var visibility: Int?
    var weather: [Weather]
    var wind: Wind?
    
    enum CodingKeys: String, CodingKey {
        case base
        case clouds
        case cod
        case coord
        case dt
        case id
        case main
        case name
        case sys
        case timezone
        case visibility
        case weather
        case wind
    }
    
    var _dateFormater: DateFormatter? = nil

    var dateFormatter: DateFormatter {
        get {
            guard let df = _dateFormater else {
                let _dateFormater = DateFormatter()
                _dateFormater.timeStyle = DateFormatter.Style.short //Set time style
                _dateFormater.dateStyle = DateFormatter.Style.none //Set date style
                _dateFormater.timeZone = .current
                return _dateFormater
            }
            return df
        }
        
    }
    
    func getCityName() -> String {
        return name ?? ""
    }
    
    func getTemperature() -> String {
        guard let temp = main?.temp else { return "" }
        let celsius = (temp-273.15).rounded(.down)
        return String(describing: celsius) + " °C"
    }
    
    func getWind() -> String {
        guard let wind = wind?.speed else { return "" }
        return String(describing: wind) + " m/s"
    }
    
    func getPressure() -> String {
        guard let pressure = main?.pressure else { return "" }
        return String(describing: pressure) + " hPa"
    }
    
    func getSunrise() -> String {
        guard let value = sys?.sunrise else { return "" }
        return dateFormatter.string(from: Date(timeIntervalSince1970: Double(value)))
    }

    func getSunset() -> String {
        guard let value = sys?.sunset else { return "" }
        return dateFormatter.string(from: Date(timeIntervalSince1970: Double(value)))
    }
}
