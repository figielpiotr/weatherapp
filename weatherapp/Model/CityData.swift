import Foundation

// MARK: - CityDatum
struct CityDatum: Codable {
    let id: Int
    let name, country: String
    let coord: Coord
}

typealias CityData = [CityDatum]
