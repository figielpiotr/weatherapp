//
//  ObjectSavableProtocolo.swift
//  weatherapp
//
//  Created by Piotr Figiel on 26/08/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import Foundation

protocol ObjectSavable {
    func setObject<Object>(_ object: Object, forKey: String) throws where Object: Encodable
    func getObject<Object>(forKey: String, castTo type: Object.Type) throws -> Object where Object: Decodable
}
