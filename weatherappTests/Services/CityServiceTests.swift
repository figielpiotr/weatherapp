//
//  CityServiceTests.swift
//  weatherappTests
//
//  Created by Piotr Figiel on 17/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

@testable import weatherapp
import XCTest
import CoreData
import CoreLocation

class CityServiceTests: XCTestCase {

    var sut: CityService!

    var coreDataStack: CoreDataTestStack!
    
    var testBundle: Bundle!
    var resourceName = "test.city.list"
    

    // MARK: - Lifecycle

    override func setUp() {
        super.setUp()

        coreDataStack = CoreDataTestStack()
        testBundle = Bundle(for: type(of: self))
        sut = CityService(backgroundContext: coreDataStack.backgroundContext, mainContext: coreDataStack.mainContext, bundle: testBundle, resource: resourceName)
        
    }
    
    override func tearDown() {
        sut = nil
        coreDataStack = nil
    }

    func test_init_contexts() {
        XCTAssertEqual(sut.backgroundContext, coreDataStack.backgroundContext)
        XCTAssertEqual(sut.mainContext, coreDataStack.mainContext)
    }
    
    func test_createCity_cityCreated() {
        //GIVEN
        let performAndWaitExpectation = expectation(description: "background perform and wait")
        coreDataStack.backgroundContext.expectation = performAndWaitExpectation

        //WHEN
        sut.addCity(id: 1, name: "Krakow", country: "PL", lon: 54.23, lat: 21.32)

        //THEN
        waitForExpectations(timeout: 1) { (_) in
            let request = NSFetchRequest<NSFetchRequestResult>.init(entityName: "City")
            let cities = try! self.coreDataStack.backgroundContext.fetch(request)

            guard let _ = cities.first else {
                XCTFail("City Missing")
                return
            }

            XCTAssertEqual(cities.count, 1)
            XCTAssertTrue(self.coreDataStack.backgroundContext.saveWasCalled)
        }
    }

    func test_cityDbEmpty_isCityDatabaseEmptyReturnTrue() {
        //GIVEN databas is empty
        //WHEN
        let isEmpty = sut.isCityDatabaseEmpty()
        //THEN
        XCTAssertTrue(isEmpty)
    }
    
    func test_cityDbIsNotEmpty_isCityDatabaseEmptyReturnFalse() {
        //GIVEN databas is not empty
        sut.addCity(id: 1, name: "Krakow", country: "PL", lon: 54.23, lat: 21.32)
        sut.addCity(id: 2, name: "something else", country: "whateveristan", lon: 54.23, lat: 21.32)
        //WHEN
        let isEmpty = self.sut.isCityDatabaseEmpty()
        //THEN
        XCTAssertFalse(isEmpty)
    }
   
    func test_cityDatabaseFileNotExists_Error() {
        //GIVEN
        sut = nil
        resourceName = "somenotexistinggibberish"
        self.setUp()
        var thrownError: AppError?
        //WHEN
        XCTAssertThrowsError(try sut.configureCitiesDatabase()) {
            thrownError = $0 as? AppError
        }
        XCTAssertTrue(thrownError == AppError.JSONdbFileNotFound)
    }
    
    func test_cityDatabaseFileMalformed_Error() {
        //GIVEN
        sut = nil
        resourceName = "brokenJSONtest.city.list"
        self.setUp()
        var thrownError: AppError?
        //WHEN
        XCTAssertThrowsError(try sut.configureCitiesDatabase()) {
            thrownError = $0 as? AppError
        }
        XCTAssertTrue(thrownError == AppError.JSONfileCouldNotBeParsed)
    }
    
    func test_configureCityDatabase_databaseHasCityEntries() {
        //GIVEN
        
        //WHEN
        try? sut.configureCitiesDatabase()
        
        //THEN
        //6 wpisów
        let isEmpty = self.sut.isCityDatabaseEmpty()
        let request = NSFetchRequest<NSFetchRequestResult>.init(entityName: "City")
        let cities = try! self.coreDataStack.backgroundContext.fetch(request)

        XCTAssertFalse(isEmpty)
        XCTAssertEqual(cities.count, 7)
    }

    func test_searchForCityFullName_searchedCityIsReturned() {
        //GIVEN
        try? sut.configureCitiesDatabase()
        let cityName = "Nurabad"
        //WHEN
        let cities = sut.searchCities(query: cityName, location: nil)
        //then
        let filteredCities = cities.filter( { $0.name == cityName} )
        XCTAssertTrue(filteredCities.count == 1)
    }
    
    func test_searchForCityPartName_searchedCityIsReturned() {
        //GIVEN
        try? sut.configureCitiesDatabase()
        let cityPartName = "Nur"
        let expectedCityName = "Nurabad"
        //WHEN
        let cities = sut.searchCities(query: cityPartName, location: nil)
        //then
        let filteredCities = cities.filter( { $0.name == expectedCityName} )
        XCTAssertTrue(filteredCities.count == 1)
    }
    
    func test_searchForAnotherCity_noOtherCitiesIsReturned() {
        //GIVEN
        try? sut.configureCitiesDatabase()
        let searchedCityName = "Azadshahr"
        let notExpectedCityName = "Nurabad"
        //WHEN
        let cities = sut.searchCities(query: searchedCityName, location: nil)
        //then
        let filteredCities = cities.filter( { $0.name == notExpectedCityName} )
        XCTAssertTrue(filteredCities.count == 0)
    }
    
    func test_searchForCityPartNameWhenFewDifferentCitiesWithSameName_citiesAreReturned() {
        //GIVEN
        try? sut.configureCitiesDatabase()
        let partOfCityName = "Qar"
        //WHEN
        let cities = sut.searchCities(query: partOfCityName, location: nil)
        //then
        XCTAssertTrue(cities.count == 2)
        let QarchakasdfasdfCity = cities.filter( { $0.name == "Qarchakasdfasdf" } )
        XCTAssertTrue(QarchakasdfasdfCity.count == 1)
        let QarchakCity = cities.filter( { $0.name == "Qarchak" } )
        XCTAssertTrue(QarchakCity.count == 1)
    }
    
    func test_searchForCities_citiesShouldBeSortedByDistanceFromGivenLocation() {
        //GIVEN
        try? sut.configureCitiesDatabase()
        let location = CLLocation(latitude: CLLocationDegrees(exactly: 34.383801)!, longitude: CLLocationDegrees(exactly: 47.055302)!)
        let expectedOrder = ["Kahriz", "Nurabad", "Azadshahr", "Qarchakasdfasdf", "Qarchak", "Istgah-e Garmsar", "Protaras"]
        //WHEN
        let cities = sut.searchCities(query: "", location: location)
        let cityNames = cities.map( {$0.name!} )
        //THEN
        XCTAssertTrue(cityNames == expectedOrder)
        
        
    }
}

