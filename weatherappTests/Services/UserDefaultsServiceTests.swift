//
//  UserDefaultsServiceTests.swift
//  weatherappTests
//
//  Created by Piotr Figiel on 26/08/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import XCTest
@testable import weatherapp


class UserDefaultsServiceTests: XCTestCase {

    var sut: UserDefaultsService!
    var userDefaults: UserDefaults!
    
// MARK: - Lifecycle

    override func setUp() {
        super.setUp()
        userDefaults = UserDefaults.standard
        clearUserDefaults()
        sut = UserDefaultsService.shared
        
    }
    
    override func tearDown() {
        sut = nil
        clearUserDefaults()
        userDefaults = nil
    }
    
//MARK: - Tests
    func test_saveCoordinatesInvoked_CoordinatesSavedInUserDefaults() {
        //GIVEN
        let coord = Coord(lon: 10, lat: 5)
        //WHEN
        sut.saveCoordinates(coord: coord)
        //THEN
        let savedData = try! userDefaults.getObject(forKey: "savedCoords", castTo: Coord.self)
        XCTAssertTrue(coord == savedData)
    }
    
    func test_getSavedCoordinatesInvoked_CoordinatesReceived() {
        //GIVEN
        let coord = Coord(lon: 15, lat: 10)
        try! userDefaults.setObject(coord, forKey: "savedCoords")
        //WHEN
        let savedCoord = sut.getSavedCoordinates()
        //THEN
        XCTAssertTrue(coord == savedCoord)
    }
    
    func test_removeSavedCoordinatesInvoked_CoordinatesRemoved() {
        //GIVEN
        let coord = Coord(lon: 15, lat: 10)
        try! userDefaults.setObject(coord, forKey: "savedCoords")
        //WHEN
        sut.removeSavedCoordinate()
        //THEN
        XCTAssertThrowsError(try userDefaults.getObject(forKey: "savedCoords", castTo: Coord.self))
    }
}
