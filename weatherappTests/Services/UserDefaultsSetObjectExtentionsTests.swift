//
//  UserDefaultsSetObjectExtentionsTests.swift
//  weatherappTests
//
//  Created by Piotr Figiel on 26/08/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import XCTest
@testable import weatherapp

class UserDefaultsSetObjectExtentionsTests: XCTestCase {
    
    var sut: UserDefaults!
    
    var key: String!
    
    override func setUp() {
        super.setUp()
        key = String(Int.random(in: 1000...9999))
        sut = UserDefaults.standard
        clearUserDefaults()
    }
    
    override func tearDown() {
        clearUserDefaults()
        sut = nil
        key = nil
    }
    
    func test_UserDefaultsConformsToObjectSavable() {
        XCTAssertNotNil((sut as? ObjectSavable))
    }
    
    func test_saveObjectInvoked_dataIsSaved() {
        //given
        let fakeData = SomeFakeDataCodable()
        //when
        try! sut.setObject(fakeData, forKey: key)
        //then
        let savedData = sut.data(forKey: key)
        XCTAssertNotNil(savedData)
        
        let decoder = JSONDecoder()
        if let _ = try? decoder.decode(SomeFakeDataCodable.self, from: savedData!) {
            XCTAssertTrue(true)
        } else {
            XCTAssertTrue(false)
        }
    }
    
    func test_getObjectInvoked_dataIsReturned() {
        //given
        let fakeData = SomeFakeDataCodable()
        let encoder = JSONEncoder()
        let data = try! encoder.encode(fakeData)
        sut.set(data, forKey: key)
        //when
        let savedData = try! sut.getObject(forKey: "test", castTo: SomeFakeDataCodable.self)
        //then
        XCTAssertNotNil(savedData)
        XCTAssertTrue(savedData.attribute1 == 1)
        XCTAssertTrue(savedData.attribute2 == 2.3)
        XCTAssertTrue(savedData.attribute3 == "test")
        
    }
    
    func test_saveObjectInvokedOnNonCodable_errorThrown(){
        
    }
    
    func test_getObjectInvokedWhenNoData_errorThrown(){
        
    }
    
    func test_getObjectInvokedWithWrongDataType_errorThrown(){
        
    }
    
        //TODO: test the UserDefaults Extension
    //    case unableToEncode = "Unable to encode object into data"
    //    case noValue = "No data object found for the given key"
    //    case unableToDecode = "Unable to decode object into given type"
}

class SomeFakeDataCodable: Codable {
    var attribute1: Int = 1
    var attribute2: Double = 2.3
    var attribute3: String = "test"
}
