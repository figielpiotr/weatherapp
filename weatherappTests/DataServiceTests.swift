//
//  DataServiceTests.swift
//  weatherappTests
//
//  Created by Piotr Figiel on 03/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import XCTest
import Mocker
@testable import weatherapp

struct ProperResponse {
    static let originalURL = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=50.0&lon=19.0&appid=a5b934e94bf3b6d9477c545ebba81733")!
    static let mockData = "{\"coord\":{\"lon\":19.95,\"lat\":50.06},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03d\"}],\"base\":\"stations\",\"main\":{\"temp\":292.82,\"feels_like\":288.28,\"temp_min\":292.04,\"temp_max\":293.71,\"pressure\":1016,\"humidity\":55},\"visibility\":10000,\"wind\":{\"speed\":6.7,\"deg\":250},\"clouds\":{\"all\":40},\"dt\":1594201427,\"sys\":{\"type\":1,\"id\":1701,\"country\":\"PL\",\"sunrise\":1594176030,\"sunset\":1594234184},\"timezone\":7200,\"id\":3085041,\"name\":\"Śródmieście\",\"cod\":200}".data(using: String.Encoding.utf8)!
    static let expectedData = WeatherData(
        base: "stations",
        clouds: Cloud(all: 40),
        cod: 200,
        coord: Coord(lon: 19.95, lat: 50.06),
        dt: 1594201427,
        id: 3085041,
        main: Main(feels_like: 288.28, humidity: 55, pressure: 1016, temp: 292.82, temp_max: 293.71, temp_min: 292.04),
        name: "Śródmieście",
        sys: Sys(country: "PL", id: 1701, sunrise: 1594176030, sunset: 1594234184, type: 1),
        timezone: 7200,
        visibility: 10000,
        weather: [Weather(description: "scattered clouds", icon: "03d", id: 802, main: "Clouds")],
        wind: Wind(deg: 250, speed: 6.7)
    )
}

struct EmptyResponse {
    static let originalURL = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=50.0&lon=19.0&appid=a5b934e94bf3b6d9477c545ebba81733")!
    static let mockData = " ".data(using: String.Encoding.utf8)!
}

struct CorruptedResponse {
    static let originalURL = URL(string: "https://api.openweathermap.org/data/2.5/weather?lat=50.0&lon=19.0&appid=a5b934e94bf3b6d9477c545ebba81733")!
    static let mockData = "{\"coord\":{\"lon\":19.95,\"lat\":50.06},\"weather\":[{\"id\":802,\"main\":\"Clouds\",\"description\":\"scattered clouds\",\"icon\":\"03d\"}],\"base\":\"stations\",\"main\":{\"temp\":292.82,\"feels_like\":288.28,\"temp_min\":292.04,\"temp_max\":293.71,\"pressure\":1016,\"humidity\":55},\"visibility\":10000,\"wind\":{\"speed\":6.7,\"deg\":250},\"clouds\":{\"all\":40},\"dt\":1594201427,\"sys\":{\"type\":1,\"idie\",\"cod\":200}".data(using: String.Encoding.utf8)!
}


class DataServiceTests: XCTestCase {

    var sut: DataService?
    var configuration : URLSessionConfiguration?
    
    override func setUpWithError() throws {
        super.setUp()
        configuration = URLSessionConfiguration.default
        configuration?.protocolClasses = [MockingURLProtocol.self]
    }

    override func tearDownWithError() throws {
        sut = nil
        configuration = nil
    }
    
    func setupSession() {
        let session = URLSession(configuration: configuration!)
        sut = DataService(session: session)
    }

    func testProperWeatherDataIsReturnedExample() throws {
        //Given
        setupSession()
        let mock = Mock(url: ProperResponse.originalURL, dataType: .json, statusCode: 200, data: [
            .get : ProperResponse.mockData // Data containing the JSON response
        ])
        mock.register()
        let e = expectation(description: "Request should finish")
        let expectedWeatherDataResponse = ProperResponse.expectedData

        //When
        sut?.getWeatherFor(lat: 50.0, lon: 19.0, onComplete: { (result, error) in
            //Then
            XCTAssertNil(error)
            XCTAssertEqual(result, expectedWeatherDataResponse)
            e.fulfill()
        })
        wait(for: [e], timeout: 10.0)
    }
    
    func testRequestShouldTimeoutAndGiveError() throws {
        //Given
        configuration?.protocolClasses = [TimeoutMockingURLProtocol.self]
        configuration?.timeoutIntervalForRequest = 1 // seconds
        setupSession()

        let e = expectation(description: "Request should timeout")
        let expectedError = AppError.serverIsNotResponding

        //When
        sut?.getWeatherFor(lat: 50.0, lon: 19.0, onComplete: { (result, error) in
            //Then
            XCTAssertNil(result)
            XCTAssertNotNil(error)
            XCTAssertEqual(error, expectedError)
            e.fulfill()
        })
        wait(for: [e], timeout: 10)
    }

    //cancelled, pusty response, zjebany response, niekompletne dane??? nie da się zserializować
    func testCancelledRequestShouldReturnError() {
        configuration?.protocolClasses = [TimeoutMockingURLProtocol.self]
        configuration?.timeoutIntervalForRequest = 60 // seconds
        setupSession()

        let e = expectation(description: "Request be cancelled")
        let expectedError = AppError.requestCancelled

        //When
        DispatchQueue.main.asyncAfter(deadline: .now()+1) { [weak self] in
            if let self = self {
                self.sut?.cancelRequests()
            }
        }
        sut?.getWeatherFor(lat: 50.0, lon: 19.0, onComplete: { (result, error) in
            //Then
            XCTAssertNil(result)
            XCTAssertNotNil(error)
            XCTAssertEqual(error, expectedError)
            e.fulfill()
        })
        wait(for: [e], timeout: 10)
    }

//TODO: Add the decoding error to the service
    func testEmptyResponseShouldReturnError() {
        //Given
        setupSession()
        let mock = Mock(url: EmptyResponse.originalURL, dataType: .json, statusCode: 200, data: [
            .get : EmptyResponse.mockData
        ])
        mock.register()
        let e = expectation(description: "Request should fail and return error")
        let expectedError = AppError.wrongData

        //When
        sut?.getWeatherFor(lat: 50.0, lon: 19.0, onComplete: { (result, error) in
            //Then
            XCTAssertNil(result)
            XCTAssertNotNil(error)
            XCTAssertEqual(error, expectedError)
            e.fulfill()
        })
        wait(for: [e], timeout: 10.0)
    }
    
    func testIncompleteResponseShouldReturnError() {
        //Given
        setupSession()
        let mock = Mock(url: CorruptedResponse.originalURL, dataType: .json, statusCode: 200, data: [
            .get : CorruptedResponse.mockData
        ])
        mock.register()
        let e = expectation(description: "Request should fail and return error")
        let expectedError = AppError.wrongData

        //When
        sut?.getWeatherFor(lat: 50.0, lon: 19.0, onComplete: { (result, error) in
            //Then
            XCTAssertNil(result)
            XCTAssertNotNil(error)
            XCTAssertEqual(error, expectedError)
            e.fulfill()
        })
        wait(for: [e], timeout: 10.0)
    }
}




// MARK: - Mock class for timeout testing
public final class TimeoutMockingURLProtocol: URLProtocol {
    override public func startLoading() {
        sleep(10)
    }
    
    /// Implementation does nothing, but is needed for a valid inheritance of URLProtocol.
    override public func stopLoading() {
        
    }
    
    /// Simply sends back the passed request. Implementation is needed for a valid inheritance of URLProtocol.
    override public class func canonicalRequest(for request: URLRequest) -> URLRequest {
        return request
    }

    /// Overrides needed to define a valid inheritance of URLProtocol.
    override public class func canInit(with request: URLRequest) -> Bool {
        return true
    }
}
