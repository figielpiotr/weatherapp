//
//  CoreDataServiceTest.swift
//  weatherappTests
//
//  Created by Piotr Figiel on 17/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

@testable import weatherapp
import XCTest
import CoreData

class CoreDataServiceTest: XCTestCase {

    var sut: CoreDataStack!
    
    override func setUp() {
        super.setUp()
        sut = CoreDataStack()
    }

    override func tearDownWithError() throws {
        sut = nil
    }

    //test_[unit under test]_[condition]_[expected outcome]

    func test_setup_completionCalled() {
        let e = expectation(description: "Setup complete handler called")

        sut.setup() {
            e .fulfill()
        }
        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func test_setup_persistentStoreCreated() {
        let e = expectation(description: "Setup creates persistent store")
        
        sut.setup() {
            e.fulfill()
        }
        waitForExpectations(timeout: 1) { [weak self] (_) in
            XCTAssertTrue(self!.sut.persistentContainer.persistentStoreCoordinator.persistentStores.count > 0)
        }
    }
    
    func test_setup_defaultPersistentContainerOnDisk() {
        let e = expectation(description: "Default setup persisten container on disk")
        
        sut.setup {
            e.fulfill()
        }
        waitForExpectations(timeout: 1) { (_) in
            XCTAssertEqual(self.sut.persistentContainer.persistentStoreDescriptions.first?.type, NSSQLiteStoreType)
        }
    }
    
    func test_setup_persistentContainerInMemory() {
        let e = expectation(description: "Default setup persisten container on disk")
        
        sut.setup(storeType: NSInMemoryStoreType) {
            e.fulfill()
        }
        waitForExpectations(timeout: 1) { (_) in
            XCTAssertEqual(self.sut.persistentContainer.persistentStoreDescriptions.first?.type, NSInMemoryStoreType)
        }
    }
    
    func test_backgroundContext_concurrencyType() {
        let setupExpectation = expectation(description: "background context")

        sut.setup(storeType: NSInMemoryStoreType) {
            XCTAssertEqual(self.sut.backgroundContext.concurrencyType, .privateQueueConcurrencyType)
            setupExpectation.fulfill()
        }

        waitForExpectations(timeout: 1.0, handler: nil)
    }

    func test_mainContext_concurrencyType() {
        let setupExpectation = expectation(description: "main context")

        sut.setup(storeType: NSInMemoryStoreType) { [weak self] in
            XCTAssertEqual(self!.sut.mainContext.concurrencyType, .mainQueueConcurrencyType)
            setupExpectation.fulfill()
        }

        waitForExpectations(timeout: 1.0, handler: nil)
    }
}
