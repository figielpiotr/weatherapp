//
//  UserDefaultsHelpers.swift
//  weatherappTests
//
//  Created by Piotr Figiel on 26/08/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import Foundation

func clearUserDefaults() {
    let domain = Bundle.main.bundleIdentifier!
    UserDefaults.standard.removePersistentDomain(forName: domain)
    UserDefaults.standard.synchronize()
}
