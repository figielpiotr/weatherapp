//
//  NSManagedObjectContextSpy.swift
//  weatherappTests
//
//  Created by Piotr Figiel on 21/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

import CoreData
import XCTest

class NSManagedObjectContextSpy: NSManagedObjectContext {
    var expectation: XCTestExpectation?
    
    var saveWasCalled = false
    
    // MARK: - Perform
    
    override func performAndWait(_ block: () -> Void) {
        super.performAndWait(block)
        
        expectation?.fulfill()
    }
    
    // MARK: - Save
    
    override func save() throws {
        try super.save()
        
        saveWasCalled = true
    }
}
