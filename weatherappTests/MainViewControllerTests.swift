//
//  MainViewControllerTests.swift
//  weatherappTests
//
//  Created by Piotr Figiel on 27/07/2020.
//  Copyright © 2020 Piotr Figiel. All rights reserved.
//

@testable import weatherapp
import XCTest
import CoreData
import CoreLocation

class MainViewControllerTests: XCTestCase {
    var sut: SingleWeatherViewController!
    
    override func setUp() {
        super.setUp()
        sut = SingleWeatherViewController()
        _ = sut.view
    }

    override func tearDownWithError() throws {
        sut = nil
    }
    
//MARK:- Testing init
    func test_whenVCCreated_dataServiceIsSet() {
        XCTAssertNotNil(sut.dataService)
    }
    
    func test_whenVCCreated_cityServiceIsSet() {
        XCTAssertNotNil(sut.cityService)
    }

    func test_whenVCCreated_locationServiceIsSet() {
        XCTAssertNotNil(sut.locationService)
    }
    
    func test_whenViewDidLoad_customViewDelegateIsSet() {
        XCTAssertNotNil(sut.customView.delegate)
        XCTAssertTrue(sut.customView.delegate == sut)
    }
    
    func test_whenViewDidLoadAndDBisEmpty_cityServiceConfigureCitiesIsCalled() {
        let e = expectation(description: "Configure cities database is called")
        let spy = CityServiceSpy()
        spy.configureCitiesExpectation = e
        sut = nil
        sut = SingleWeatherViewController(DataService.shared, spy)
        //sut.viewDidLoad()
        waitForExpectations(timeout: 1) { (_) in
            XCTAssert(true)
        }
    }
    
    func test_whenVCCreated_searchControllerIsSet() {
        XCTAssertNotNil(sut.searchController)
    }
    
    
    func test_whenVCCreated_searchControllerIsInactive() {
        XCTAssertFalse(sut.searchController.isActive)
    }
    
    func test_whenCVCreated_SearchBarIsSet() {
        XCTAssertNotNil(sut.searchController.searchBar)
    }
    
    func test_whenCVCreated_SearchBarDelegateIsSet() {
        XCTAssertNotNil(sut.searchController.searchBar.delegate)
    }
    
    func test_whenCVCreated_ConformsToSearchBarDelegateProtocol() {
        XCTAssert(sut.conforms(to: UISearchBarDelegate.self))
        XCTAssertTrue(self.sut.responds(to: #selector(sut.updateSearchResults(for:))))
        XCTAssertTrue(self.sut.responds(to: #selector(sut.searchBarTextDidEndEditing(_:))))
        XCTAssertTrue(self.sut.responds(to: #selector(sut.searchBarCancelButtonClicked(_:))))
    }


//MARK:- Testing Search Bar
    func test_whenVCCreated_SearchBarIsHidden() {
        XCTAssertTrue(sut.searchController.searchBar.isHidden)
    }
    
    
    
    func test_whenShowSearchBarInvoked_SearchBarShouldNotBeHidden() {
        sut.showSearchBar()
        XCTAssertFalse(sut.searchController.searchBar.isHidden)
        //TODO: not testable, missing some configuration??
        XCTAssertTrue(sut.searchController.searchBar.isFirstResponder)
    }
    
    func test_whenHideSearchBarInvoked_SearchBarShouldBeHidden() {
        sut.viewDidLoad()
        sut.showSearchBar()
        sut.hideSearchBar()
        XCTAssertTrue(sut.searchController.searchBar.isHidden)
        XCTAssertFalse(sut.searchController.isActive)
        XCTAssertFalse(sut.searchController.searchBar.isFirstResponder)
    }
    
    //test that nothing is called when no input
    
    func test_whenSearchBarUsed_CityServiceSearchCitiesIsCalled() {
        let e = expectation(description: "Search Cities was called")
        let spy = CityServiceSpy()
        spy.searchCitiesExpectation = e
        sut = nil
        sut = SingleWeatherViewController(DataService.shared, spy)
        sut.viewDidLoad()
        sut.searchController.searchBar.text = "k"
        waitForExpectations(timeout: 1) { (_) in
            XCTAssert(true)
        }
    }
    
    func test_whenSearchBarUsedButNothingInTheField_CityServiceSearchCitiesIsNotCalled() {
        let e = expectation(description: "Search Cities was not called")
        e.isInverted = true
        let spy = CityServiceSpy()
        spy.searchCitiesExpectation = e
        sut = nil
        sut = SingleWeatherViewController(DataService.shared, spy)
        sut.viewDidLoad()
        sut.searchController.searchBar.text = ""
        waitForExpectations(timeout: 1) { (_) in
            
        }
        
    }
    
    //TODO: Not testable, missing some configuration
//    func test_whenOrientationChanges_changeOrientatioOnViewIsCalled() {
////        let spy = MainViewSpy()
////        sut.view = spy
////        sut.viewDidLoad()
////        sut.viewDidAppear(false)
////        sut.willTransition(to: nil, with: nil)
////        XCTAssertTrue(spy.changeOrientationWasCalled)
//        XCTAssertTrue(false)
//    }
    
}

class CityServiceSpy: CityService {
    
    var configureCitiesExpectation: XCTestExpectation?
    var searchCitiesExpectation: XCTestExpectation?
    
    override init(backgroundContext: NSManagedObjectContext = CoreDataStack.shared.backgroundContext, mainContext: NSManagedObjectContext = CoreDataStack.shared.mainContext, bundle: Bundle = Bundle.main, resource: String = "current.city.list") {
    }
    
    override func configureCitiesDatabase() throws {
        self.configureCitiesExpectation?.fulfill()
    }
    
    override func searchCities(query: String, location: CLLocation?) -> [City] {
        //let result = super.searchCities(query: query, location: location)
        self.searchCitiesExpectation?.fulfill()
        return []
        
    }
}

class MainViewSpy: MainView {
    var changeOrientationWasCalled = false
    override func changeOrientation(orientation: UIInterfaceOrientation) {
        changeOrientationWasCalled = true
    }
}
