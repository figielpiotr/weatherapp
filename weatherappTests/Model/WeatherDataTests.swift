import XCTest
@testable import weatherapp


class WeatherDataTests: XCTestCase {

    let inputData = WeatherData(
        base: "stations",
        clouds: Cloud(all: 40),
        cod: 200,
        coord: Coord(lon: 19.95, lat: 50.06),
        dt: 1594201427,
        id: 3085041,
        main: Main(feels_like: 288.28, humidity: 55, pressure: 1016, temp: 292.82, temp_max: 293.71, temp_min: 292.04),
        name: "Krakow",
        sys: Sys(country: "PL", id: 1701, sunrise: 1594176030, sunset: 1594234184, type: 1),
        timezone: 7200,
        visibility: 10000,
        weather: [Weather(description: "scattered clouds", icon: "03d", id: 802, main: "Clouds")],
        wind: Wind(deg: 250, speed: 6.7)
    )
    
    var sut: WeatherData?
    
    override func setUpWithError() throws {
        super.setUp()
        sut = inputData
    }

    override func tearDownWithError() throws {
        sut = nil
    }
    

    func testCityNameIsReturnedExample() throws {
        guard let sut = sut else { return }
        //Given
        let expectedValue = "Krakow"
        //when
        let testedValue = sut.getCityName()
        //then
        XCTAssertEqual(expectedValue, testedValue)
    }
    
    func testTemperatureValueIsReturnedExample() throws {
        guard let sut = sut else { return }
        //Given
        let expectedValue = "292.82"
        //when
        let testedValue = sut.getTemperature()
        //then
        XCTAssertEqual(expectedValue, testedValue)
    }
    
    func testPressureValueIsReturnedExample() throws {
        guard let sut = sut else { return }
        //Given
        let expectedValue = "1016"
        //when
        let testedValue = sut.getPressure()
        //then
        XCTAssertEqual(expectedValue, testedValue)
    }
    
    func testSunsetTimeIsReturnedExample() throws {
        guard let sut = sut else { return }
        //Given
        let dateFormater = DateFormatter()
        dateFormater.timeStyle = DateFormatter.Style.short
        dateFormater.dateStyle = DateFormatter.Style.none
        dateFormater.timeZone = .current
        let expectedValue = dateFormater.string(from: Date(timeIntervalSince1970: Double(1594234184)))
        
        //when
        let testedValue = sut.getSunset()
        //then
        XCTAssertEqual(expectedValue, testedValue)
    }
    
    func testSunriseTimeIsReturnedExample() throws {
        guard let sut = sut else { return }
        //Given
        let dateFormater = DateFormatter()
        dateFormater.timeStyle = DateFormatter.Style.short
        dateFormater.dateStyle = DateFormatter.Style.none
        dateFormater.timeZone = .current
        let expectedValue = dateFormater.string(from: Date(timeIntervalSince1970: Double(1594176030)))
        
        //when
        let testedValue = sut.getSunrise()
        //then
        XCTAssertEqual(expectedValue, testedValue)
    }

    
}
